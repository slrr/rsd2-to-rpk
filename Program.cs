﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSD2ToRPK
{
  internal class Program
  {
    [STAThread]
    public static void Main(string[] args)
    {
      SlrrLib.Model.MessageLog.SetConsoleLogOutput();
      string file = "";
      if (args.Length == 0)
      {
        System.Windows.Forms.OpenFileDialog diag = new System.Windows.Forms.OpenFileDialog
        {
          InitialDirectory = System.IO.Directory.GetCurrentDirectory(),
          Filter = "*.RDB2|*.RDB2"
        };
        if (diag.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
          return;
        file = diag.FileName;
      }
      else
      {
        file = args.First();
      }
      SlrrLib.Model.RpkExchange exc = new SlrrLib.Model.RpkExchange();
      SlrrLib.Model.DynamicRpk rpkDat = null;
      try
      {
        rpkDat = exc.LoadRPKFromRDB2(file);
      }
      catch
      {
        SlrrLib.Model.MessageLog.AddError("FATAL ERROR READING");
        SlrrLib.Model.MessageLog.AddError(exc.GetLastReadLineAndIndex());
        return;
      }
      try
      {
        rpkDat.SaveAs(file.Substring(0, file.Length - 5), true, "_BAK_RDB2_");
      }
      catch (Exception e)
      {
        SlrrLib.Model.MessageLog.AddError("DIED ON SAVING RPKDATA");
        SlrrLib.Model.MessageLog.AddError(e.Message);
        SlrrLib.Model.MessageLog.AddError(e.StackTrace);
      }
    }
  }
}
